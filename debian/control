Source: sugar-log-activity
Section: x11
Priority: optional
Maintainer: Debian Sugar Team <pkg-sugar-devel@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
 Elías Alejandro Año Mendoza <ealmdz@gmail.com>,
Build-Depends:
 debhelper-compat (= 12),
 dh-linktree,
 dh-sequence-python3,
 python3,
 python3-sugar3 (>= 0.118-3~),
 unzip,
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/pkg-sugar-team/sugar-log-activity.git
Vcs-Browser: https://salsa.debian.org/pkg-sugar-team/sugar-log-activity
Homepage: https://wiki.sugarlabs.org/go/Activities/Log
Rules-Requires-Root: no

Package: sugar-log-activity
Architecture: all
Depends:
 gir1.2-glib-2.0,
 gir1.2-gtk-3.0,
 gir1.2-pango-1.0,
 python3,
 python3-gi,
 python3-sugar3,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 net-tools,
 procps,
Provides:
 ${python3:Provides},
Description: Sugar Learning Platform - log viewing and reporting activity
 Sugar Learning Platform promotes collaborative learning
 through Sugar Activities that encourage critical thinking,
 the heart of a quality education.
 Designed from the ground up especially for children,
 Sugar offers an alternative to traditional "office-desktop" software.
 .
 Learner applications in Sugar are called Activities.
 They are software packages that automatically save your work -
 producing specific instances of the Activity
 that can be resumed at a later time.
 Many Activities support learner collaboration,
 where multiple learners may be invited
 to join a collective Activity session.
 .
 Log files are created when Activities run,
 and by other Sugar-related processes.
 They list software errors, among other data,
 and can help diagnose software problems.
 .
 This package contains the Log activity to view Sugar-related log files.
